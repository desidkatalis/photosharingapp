import React from 'react'
import { View, Text } from 'react-native'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import AntIcon from 'react-native-vector-icons/Feather'

/*Feed tab*/
import Feed from './bottom-tabs/Feed'

/*Search tab*/
import Search from './bottom-tabs/Search'

/*Upload tab*/
import Upload from './bottom-tabs/Upload'

/*Profile tab*/
import UserProfile from './bottom-tabs/UserProfile'

/*Setting tab*/
import Setting from './bottom-tabs/Setting'

const FeedStack = createStackNavigator({
    Feed: { screen: Feed }
}, { initialRouteName: 'Feed' })

/*Hiding bottom bar from child screen*/
FeedStack.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }
    return { tabBarVisible };
}

const UploadStack = createStackNavigator({
    Upload: { screen: Upload }
}, { initialRouteName: 'Upload' })

const ProfileStack = createStackNavigator({
    UserProfile: { screen: UserProfile }
}, { initialRouteName: 'UserProfile' })

const SettingStack = createStackNavigator({
    Setting: { screen: Setting },
}, { initialRouteName: 'Setting' })

/*Route for Bottom tabs navigation*/
const contentNav = createMaterialBottomTabNavigator(
    {
        Feed: {
            screen: FeedStack,
            navigationOptions: {
                tabBarLabel: 'Feeds',
                tabBarIcon: ({ tintColor }) => (
                    <AntIcon style={[{ color: tintColor }]} size={25} name={'home'} />
                ),
            }
        },
        Search: {
            screen: Search,
            navigationOptions: {
                tabBarLabel: 'Search',
                tabBarIcon: ({ tintColor }) => (
                    <AntIcon style={[{ color: tintColor }]} size={25} name={'search'} />
                ),
            }
        },
        Uploads: {
            screen: UploadStack,
            navigationOptions: {
                tabBarLabel: 'Post',
                tabBarIcon: ({ tintColor }) => (
                    <AntIcon style={[{ color: tintColor }]} size={25} name={'upload'} />
                ),
            }
        },
        Profile: {
            screen: ProfileStack,
            navigationOptions: {
                tabBarLabel: 'Profile',
                tabBarIcon: ({ tintColor }) => (
                    <AntIcon style={[{ color: tintColor }]} size={25} name={'user'} />
                ),
            }
        },
        Setting: {
            screen: SettingStack,
            navigationOptions: {
                tabBarLabel: 'Setting',
                tabBarIcon: ({ tintColor }) => (
                    <AntIcon style={[{ color: tintColor }]} size={25} name={'settings'} />
                ),
            }
        },
    },
    {
        initialRouteName: 'Feed',
        activeColor: '#000',
        inactiveColor: '#727272',
        barStyle: { backgroundColor: 'lightblue' },
    }
);

export default createAppContainer(contentNav);