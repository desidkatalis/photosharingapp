import React from 'react'
import { View, Text, Image } from 'react-native'

export const ListImage = (item) => {
    return (
        <Image
            source={{ uri: `http://20.4.12.223:1999/getPost/${item.post_path}` }}
            style={{ width: 200, height: 200 }}
        />
    )
}

export const ListFeed = (item) => {
    return (
        <>
            <View>
                <Text>
                    {item.caption}
                </Text>
            </View>
        </>
    )
}
