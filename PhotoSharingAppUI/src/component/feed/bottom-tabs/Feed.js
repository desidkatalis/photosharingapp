import React, { Component } from 'react'
import { FlatList, View, Text } from 'react-native'
import { Header, ListItem } from 'react-native-elements'
import { connect } from 'react-redux'
import { getAllDataFeed } from '../../../actions/feedIndex'
import { ListImage, ListFeed } from './List'

class Feed extends Component {
    static navigationOptions = { header: null }
    constructor(props) {
        super(props);
        this.state = {
            email: '',
        }
    }

    async componentDidMount() {
        const { getAllDataFeed } = this.props
        await getAllDataFeed(`http://20.4.12.223:1999/getFeeds/${this.state.email}`)
    }
    keyExtractor = (item, index) => index.toString()

    renderItem = ({ item }) => (
        <ListItem
            title={ListImage(item)}
            subtitle={ListFeed(item)}
            bottomDivider
        />
    )

    render() {
        const { data } = this.props
        return (
            <>
                <Header
                    centerComponent={{ text: 'Feeds', style: { color: '#000' } }}
                    containerStyle={{
                        backgroundColor: 'lightblue',
                        justifyContent: 'space-around',

                    }}
                />
                <View>
                    <Text>Blank Page</Text>
                    <FlatList
                        keyExtractor={this.keyExtractor}
                        data={data}
                        renderItem={this.renderItem}
                    />
                </View>

            </>
        )
    }
}

const mapStateToProps = state => ({
    email: state.dataReducer.email,
    data: state.dataReducer.data,
});

const mapDispatchToProps = dispatch => ({
    getAllDataFeed: url => dispatch(getAllDataFeed(url))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Feed);