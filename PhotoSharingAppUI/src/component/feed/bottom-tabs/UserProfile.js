import React, { Component } from 'react'
import {
    View, Text,
    Image
} from 'react-native'
import { Card, Header, Avatar } from 'react-native-elements'
import { connect } from 'react-redux'
import { getAllDataFeed } from '../../../actions/feedIndex'
import AsyncStorage from '@react-native-community/async-storage'

class UserProfile extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);
        this.state = {
            email: ''
        }
    }

    async componentDidMount() {
        const emailStore = await AsyncStorage.getItem('email')
        this.setState({ email: emailStore })
        await this.props.getAllDataFeed(`http://20.4.12.223:1999/getUser/${this.state.email}`)
    }

    render() {
        const { data } = this.props
        return (
            <>
                <Header
                    centerComponent={{ text: 'MY ACCOUNT', style: { color: '#000' } }}
                    rightComponent={{ icon: 'menu', type: 'anticon', color: '#000' }}
                    containerStyle={{
                        backgroundColor: 'lightblue',
                        justifyContent: 'space-around',
                    }}
                />

                <View style={{ flex: 1, backgroundColor: '#e2e2e2' }}>
                    <Card>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 2, alignItems: 'center', paddingLeft: 15 }}>
                                {
                                    data.dp_path ?
                                        <Avatar rounded
                                            source={{ uri: `http://20.4.12.223:1999/getDp/${this.state.email}/display_pic.jpg` }}
                                            size='xlarge'
                                        />
                                        :
                                        <Avatar rounded
                                            size='xlarge'
                                            icon={{ name: 'user', type: 'font-awesome' }}
                                        />

                                }
                            </View>
                            <View style={{ flex: 4, marginLeft: 20, justifyContent: 'center' }}>
                                <Text>{data.emailID}</Text>
                                <Text>{data.bio}</Text>
                            </View>
                        </View>
                    </Card>

                    <Card>
                        <Text>Photo</Text>
                    </Card>
                </View>
            </>
        )
    }
}

const mapStateToProps = state => ({
    data: state.dataReducer.data,
    email: state.dataReducer.email
});

const mapDispatchToProps = dispatch => ({
    getAllDataFeed: url => dispatch(getAllDataFeed(url))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(UserProfile);