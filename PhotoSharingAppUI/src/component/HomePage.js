import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import styles from '../styles/style'

class HomePage extends Component {
    static navigationOptions = { header: null }
    render() {
        const { navigate } = this.props.navigation
        return (
            <>
                <View style={styles.container}>
                    <Text style={styles.txtTitle}>Welcome to the Apps!</Text>
                    <Image style={styles.inputLogo} source={require('../styles/image/photo-sharing.png')} />

                    <View style={styles.buttomContainer}>
                        <TouchableOpacity style={styles.buttonContainer}
                            onPress={() => navigate('Login')}
                        >
                            <Text style={styles.buttonnTxt}>Sign In</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.buttonContainer}
                            onPress={() => navigate('SignUp')}
                        >
                            <Text style={styles.buttonTxt}>Sign Up</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </>
        )
    }
}

export default HomePage