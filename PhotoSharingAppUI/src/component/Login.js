import React, { Component } from 'react'
import { TouchableOpacity, Text, View, TextInput, Image } from 'react-native'
import { loginUser } from '../actions/userIndex'
import { connect } from 'react-redux'
import styles from '../styles/style'
import { email, password } from '../validator/constraints'

class Login extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            emailValidation: false,
            passwordValidation: false,
        }
    }

    checkEmail = () => {
        return this.state.emailValidation ? email.test(this.state.email) : !this.state.emailValidation
    }

    checkPassword = () => {
        return this.state.passwordValidation ? password.test(this.state.password) : !this.state.passwordValidation
    }

    checkDisable = () => {
        if (
            email.test(this.state.email) &&
            password.test(this.state.password)
        ) {
            return false
        } else {
            return true
        }
    }

    login = () => {
        const { email, password } = this.state
        this.props.loginUser(email, password)
        this.setState({
            clickPressed: true
        })
        this.props.navigation.navigate('Feed', { email: email })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.txtHeader}> Sign In </Text>
                </View>
                <View style={styles.inputContainer}>
                    <Image style={styles.inputIcon} source={require('../styles/image/email.png')} />
                    <TextInput
                        style={styles.inputs}
                        placeholder="Email"
                        keyboardType="email-address"
                        underlineColorAndroid='transparent'
                        autoCapitalize='none'
                        onChangeText={(emailInput) => this.setState({ email: emailInput })}
                        onBlur={() => {
                            this.setState({ emailValidation: true })
                        }}
                        returnKeyType='next'
                    />
                    {this.checkEmail() ? null :
                        <Text style={styles.bottomWarning}>
                            Please check your domain email!
                        </Text>}
                </View>

                <View style={styles.inputContainer}>
                    <Image style={styles.inputIcon} source={require('../styles/image/password.png')} />
                    <TextInput
                        style={styles.inputs}
                        placeholder="Password"
                        secureTextEntry={true}
                        autoCapitalize='none'
                        underlineColorAndroid='transparent'
                        onChangeText={(passwordInput) => this.setState({ password: passwordInput })}
                        onBlur={() => {
                            this.setState({ passwordValidation: true })
                        }}
                        returnKeyType='done'
                        secureTextEntry={true}
                    />
                    {this.checkPassword() ? null :
                        <Text style={style.bottomWarning}>
                            Password must be have at least 6 character!
                        </Text>}
                </View>

                <View style={this.checkDisable() ? [styles.pressButton, styles.disabled] : [styles.buttonContainer, styles.signinButton]}>
                    <TouchableOpacity disabled={this.checkDisable()} onPress={this.login}>
                        <Text style={styles.signinText}>Login</Text>
                    </TouchableOpacity>
                </View>

                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('SignUp')}>
                    <Text style={styles.navigateSignup}>Don't have an account? Sign Up here!</Text>
                </TouchableOpacity>

            </View >
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        loginUser: (email, password) => {
            dispatch(loginUser(email, password));
        }
    };
};
const mapStateToProps = (state) => {
    return {
        dataReducer: state.dataReducer
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)