import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SignUp from './SignUp';
import Login from './Login';
import Profile from './Profile'
import HomePage from './HomePage'
import ContentNavigator from '../component/feed/ContentNavigator'

const MainNavigator = createStackNavigator({
    HomePage: { screen: HomePage },
    SignUp: { screen: SignUp },
    Login: { screen: Login },
    Profile: { screen: Profile },
    ContentNavigator: { screen: ContentNavigator }
},
    {
        initialRouteName: 'HomePage'
    });


const Container = createAppContainer(MainNavigator);

export default Container;