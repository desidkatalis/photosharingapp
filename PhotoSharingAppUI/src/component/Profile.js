import React, { Component } from 'react'
import {
    Image,
    Text,
    View,
    TextInput,
    TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import styles from '../styles/style'
import { name, username } from '../validator/constraints'
import ImagePicker from 'react-native-image-picker';
import { updateUserHandler } from '../actions/userIndex'

class Profile extends Component {
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            firstName: '',
            lastName: '',
            username: '',
            password: '',
            buttonPressed: false,
            filePath: {},
            photoData: ''
        }
    }

    chooseFile = () => {
        var options = {
            title: 'Select Image',
            maxWidth: 600,
            maxHeight: 600,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ');
            } else {
                this.setState({
                    filePath: response,
                    photoData: response.data
                });
            }
        });
    };

    checkFirstName = () => {
        return this.state.validFirstName ? name.test(this.state.firstName) : !this.state.validFirstName
    }

    checkLastName = () => {
        return this.state.validLastName ? name.test(this.state.lastName) : !this.state.validLastName
    }

    checkUsername = () => {
        return this.state.validUserName ? username.test(this.state.username) : !this.state.validUserName
    }

    saveProfile = () => {
        const { firstName, lastName, username, photoData } = this.state
        const email = this.props.navigation.getParam('email')
        const payload = {
            email: email,
            username: username,
            firstName: firstName,
            lastName: lastName,
            photoData: photoData
        }
        this.props.updateUserHandler(payload)
        this.setState({
            buttonPressed: true
        })
        this.props.navigation.navigate('Login')
    }

    checkDisable = () => {
        if (
            name.test(this.state.firstName) &&
            name.test(this.state.lastName) &&
            username.test(this.state.username)
        ) {
            return false
        } else {
            return true
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.txtHeader}>Create Profile</Text>
                <TouchableOpacity onPress={() => this.chooseFile()}>
                    {this.state.filePath.uri ?
                        <Image
                            source={{ uri: this.state.filePath.uri }}
                            style={styles.imageProfile}
                        /> :
                        <Image style={styles.profile} source={require('../styles/image/profile.png')} />
                    }
                </TouchableOpacity>

                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.inputs}
                        placeholder="Input your First Name"
                        underlineColorAndroid='transparent'
                        onChangeText={(inputFirstName) => this.setState({ firstName: inputFirstName })}
                        value={this.state.firstName}
                    />
                    {this.checkFirstName() ? null :
                        <Text style={styles.bottomWarning}>
                            First name must be capitalize and not contain space!
                        </Text>}
                </View>

                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.inputs}
                        placeholder="Input your Last Name"
                        underlineColorAndroid='transparent'
                        onChangeText={(inputLastName) => this.setState({ lastName: inputLastName })}
                        value={this.state.lastName}
                    />
                    {this.checkLastName() ? null :
                        <Text style={style.bottomWarning}>
                            Last name must be capitalize and not contain space!
                        </Text>}
                </View>

                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.inputs}
                        placeholder="Input your Username"
                        underlineColorAndroid='transparent'
                        onChangeText={(inputUsername) => this.setState({ username: inputUsername })}
                        value={this.state.username}
                    />
                    {this.checkUsername() ? null :
                        <Text style={style.bottomWarning}>
                            Just using A-Z,a-z,0-10 and not space!
                        </Text>}
                </View>

                <View style={this.checkDisable() ? [styles.pressButton, styles.disabled] : [styles.buttonContainer, styles.signinButton]}>
                    <TouchableOpacity disabled={this.checkDisable()} onPress={this.saveProfile}>
                        <Text style={styles.signinText}>Save Profile</Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateUserHandler: (payload) => {
            dispatch(updateUserHandler(payload));
        }
    }
};

const mapStateToProps = (state) => {
    return {
        dataReducer: state.dataReducer
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile)