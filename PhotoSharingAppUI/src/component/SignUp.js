import React, { Component } from 'react';
import {
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image
} from 'react-native';
import { email, password } from '../validator/constraints'
import { createNewUserHandler } from '../actions/userIndex'
import { connect } from 'react-redux'
import styles from '../styles/style'

class SignUp extends Component {
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            repassword: '',
            buttonPressed: false,
        }
    }

    checkEmail = () => {
        return this.state.emailValid ? email.test(this.state.email) : !this.state.emailValid
    }

    checkPassword = () => {
        return this.state.passwordValid ? password.test(this.state.password) : !this.state.passwordValid
    }

    checkRepassword = () => {
        return this.state.repasswordValid ? this.state.password === this.state.repassword : !this.state.repasswordValid
    }

    checkDisable = () => {
        if (
            email.test(this.state.email) &&
            password.test(this.state.password) &&
            this.state.password === this.state.repassword
        ) {
            return false
        } else {
            return true
        }
    }

    signUpButton = () => {
        const { email, password } = this.state
        this.props.createNewUserHandler(email, password)
        this.setState({
            buttonPressed: true
        })
        this.props.navigation.navigate('Profile', { email: email })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.txtHeader}> Sign Up </Text>
                </View>
                <View style={styles.inputContainer}>
                    <Image style={styles.inputIcon} source={require('../styles/image/email.png')} />
                    <TextInput
                        style={styles.inputs}
                        placeholder="Input your Email"
                        keyboardType="email-address"
                        underlineColorAndroid='transparent'
                        autoCapitalize='none'
                        onChangeText={(emailInput) => this.setState({ email: emailInput })}
                        value={this.state.email}
                        ref={(input) => { this.passwordType = input; }}
                        onBlur={() => {
                            this.setState({ emailValid: true })
                        }}
                        onSubmitEditing={() => {
                            this.passwordType.focus();
                        }}
                        returnKeyType='next' />
                    {this.checkEmail() ? null :
                        <Text style={styles.bottomWarning}>
                            Please check your domain email!
                        </Text>}
                </View>

                <View style={styles.inputContainer}>
                    <Image style={styles.inputIcon} source={require('../styles/image/password.png')} />
                    <TextInput
                        style={styles.inputs}
                        placeholder="Input your Password"
                        secureTextEntry={true}
                        autoCapitalize='none'
                        underlineColorAndroid='transparent'
                        onChangeText={(passwordInput) => this.setState({ password: passwordInput })}
                        ref={(input) => { this.passwordType = input; }}
                        onBlur={() => {
                            this.setState({ passwordValid: true })
                        }}
                        onSubmitEditing={() => {
                            this.repasswordType.focus();
                        }}
                        returnKeyType='next'
                        secureTextEntry={true} />
                    {this.checkPassword() ? null :
                        <Text style={styles.bottomWarning}>
                            Password must be have at least 6 character!
                        </Text>}
                </View>

                <View style={styles.inputContainer}>
                    <Image style={styles.inputIcon} source={require('../styles/image/password.png')} />
                    <TextInput
                        style={styles.inputs}
                        placeholder="Re-enter your Password"
                        secureTextEntry={true}
                        autoCapitalize='none'
                        underlineColorAndroid='transparent'
                        onChangeText={repassword => this.setState({ repassword })}
                        ref={(input) => { this.repasswordType = input; }}
                        onBlur={() => {
                            this.setState({ repasswordValid: true })
                        }}
                        returnKeyType='done'
                        secureTextEntry={true}
                    />
                    {this.checkRepassword() ? null :
                        <Text style={styles.bottomWarning}>
                            Password does not match! Please check again.
                        </Text>}
                </View>

                <View style={this.checkDisable() ? [styles.pressButton, styles.disabled] : [styles.buttonContainer, styles.signupButton]}>
                    <TouchableOpacity disabled={this.checkDisable()} onPress={this.signUpButton}>
                        <Text style={styles.signUpText}>Sign up</Text>
                    </TouchableOpacity>
                </View>

                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Login')}>
                    <Text style={styles.navigateSignin}>Already have an account? Sign In here</Text>
                </TouchableOpacity>

            </View >
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        createNewUserHandler: (email, password) => {
            dispatch(createNewUserHandler(email, password));
        }
    };
};
const mapStateToProps = (state) => {
    return {
        dataReducer: state.dataReducer
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SignUp)
