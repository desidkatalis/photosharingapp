import { combineReducers } from 'redux';
import SignupReducer from './SignupReducer';

const allReducers = combineReducers({
    dataReducer: SignupReducer,
});

const rootReducer = (state, action) => {
    return allReducers(state, action)
}

export default rootReducer;