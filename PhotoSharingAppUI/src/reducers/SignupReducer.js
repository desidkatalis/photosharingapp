import {
    GET_FORM_SIGNUP_STARTED,
    GET_FORM_SIGNUP_FAILURE,
    GET_FORM_SIGNUP_SUCCESS,
    GET_FEEDS_SUCCESS,
    POST_FEEDS_SUCCESS,
    PUT_FEEDS_SUCCESS,
    AUTHENTICATION_DATA,
    DATA_FEEDS_FAILURE
} from '../actions/types';

const initialState = {
    isLoading: false,
    data: '',
    photoSharingApp: [],
    error: null,
    email: ''
};

const SignupReducers = (state = initialState, action) => {
    switch (action.type) {
        case GET_FORM_SIGNUP_STARTED:
            return {
                ...state,
                isLoading: true
            };
        case GET_FORM_SIGNUP_SUCCESS:
            return {
                ...state,
                isLoading: true,
                error: null,
                photoSharingApp: action.payload
            }
        case GET_FORM_SIGNUP_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.payload.error
            }
        case GET_FEEDS_SUCCESS:
            return {
                ...state,
                data: action.data
            }
        case POST_FEEDS_SUCCESS:
            return {
                ...state,
                data: action.payload,
                isLoading: true,
                error: action.error
            }
        case PUT_FEEDS_SUCCESS:
            return {
                ...state,
                data: action.payload,
                isLoading: true
            }
        case DATA_FEEDS_FAILURE:
            return {
                ...state,
                error: action.payload.error
            }
        case AUTHENTICATION_DATA:
            return {
                ...state,
                email: action.email
            }
        default:
            return state
    }
}

export default SignupReducers