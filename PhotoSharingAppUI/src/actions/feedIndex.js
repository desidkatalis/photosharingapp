import {
    GET_FEEDS_SUCCESS,
    POST_FEEDS_SUCCESS,
    PUT_FEEDS_SUCCESS,
    AUTHENTICATION_DATA,
    DATA_FEEDS_FAILURE
} from './types'
import axios from 'axios'

export const getFeedsSuccess = data => ({
    type: GET_FEEDS_SUCCESS,
    data: data
})

export const postFeedsSuccess = response => ({
    type: POST_FEEDS_SUCCESS,
    payload: response
})

export const putFeedsSuccess = response => ({
    type: PUT_FEEDS_SUCCESS,
    payload: response
})

export const authDataSuccess = email => ({
    type: AUTHENTICATION_DATA,
    email: email
})

export const dataFeedsFailure = error => ({
    type: DATA_FEEDS_FAILURE,
    payload: error
})

export const getAllDataFeed = () => {
    return dispatch => {
        axios
            .get(`http://20.4.12.223:1999/api/feed/{email}`)
            .then(response => {
                dispatch(getFeedsSuccess(response.data));
            })
            .catch(error => {
                dispatch(dataFeedsFailure(error));
            });
    };
};

export const postDataFeed = (payload) => {
    return dispatch => {
        axios.post(`http://20.4.12.223/api/feed/{email}`, payload)
            .then(response => {
                dispatch(postFeedsSuccess(response.data))
            })
            .catch(error => {
                dispatch(dataFeedsFailure(error))
            })
    }
}

export const putDataFeed = (payload) => {
    return dispatch => {
        axios
            .put(`http://20.4.12.223:1999/api/user`, payload)
            .then(response => {
                dispatch(putFeedsSuccess(response))
            })
            .catch(error => {
                dispatch(dataFeedsFailure(error))
            })
    }
}

export const authDataEmail = (email) => {
    return dispatch => {
        dispatch(authDataSuccess(email))
    }
}