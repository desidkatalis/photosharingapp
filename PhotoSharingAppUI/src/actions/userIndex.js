import {
    GET_FORM_SIGNUP_STARTED,
    GET_FORM_SIGNUP_FAILURE,
    GET_FORM_SIGNUP_SUCCESS
} from './types';
import axios from 'axios'

export const getFormSignupStarted = () => {
    return {
        type: GET_FORM_SIGNUP_STARTED,
    };
}

export const getFormSignupFailure = error => {
    return {
        type: GET_FORM_SIGNUP_FAILURE,
        payload: {
            error
        }
    };
}

export const getFormSignupSuccess = registration => {
    return {
        type: GET_FORM_SIGNUP_SUCCESS,
        payload: registration,
    };
}

const checkUserHandler = () => {
    return (dispatch) => {
        axios
            .get(`http://20.4.12.223:1999/api/user`)
            .then(response => {
                dispatch(getFormSignupSuccess(response.data))
            })
            .catch(error =>
                dispatch(getFormSignupFailure(error))
            )
    }
}

const createNewUserHandler = (email, password) => {
    return (dispatch) => {
        axios.post(`http://20.4.12.223:1999/api/user`,
            {
                email: email,
                password: password,
            }
        )
            .then(response => {
                dispatch(getFormSignupSuccess(response.data))
            })
            .catch(error =>
                dispatch(getFormSignupFailure(error))
            )
    }
}

const loginUser = (email, password) => {
    return dispatch => {
        dispatch(getFormSignupStarted())
        axios
            .post(`http://20.4.12.223/api/user/login`,
                {
                    email: email,
                    password: password,
                }
            )
            .then(response => {
                dispatch(getFormSignupSuccess(response.data))
            })
            .catch(error =>
                dispatch(getFormSignupFailure(error))
            )
    }
}

const updateUserHandler = (payload) => {
    return (dispatch) => {
        axios.put(`http://20.4.12.223:1999/api/user`, payload)
            .then(response => {
                dispatch(getFormSignupSuccess(response))
            })
            .catch(error => {
                dispatch(getFormSignupFailure(error))
                console.log(error)

            })
    }
}

export { checkUserHandler, createNewUserHandler, updateUserHandler, loginUser }