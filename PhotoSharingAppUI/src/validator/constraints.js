const email = /^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+.)?[a-zA-Z]+.)?(RealUniversity).ac.id$/
const password = /^[a-zA-Z0-9]{6,}$/
const name = /^[A-Z]{1}[a-z]{0,20}$/
const username = /^[a-zA-Z0-9]{1,10}$/

module.exports = { email, password, name, username }