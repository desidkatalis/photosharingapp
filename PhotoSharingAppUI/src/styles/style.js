import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'lightblue',
    },
    pressButton: {
        alignSelf: 'center',
        backgroundColor: '#fff',
        borderRadius: 20,
        width: '60%',
        height: 40,
        justifyContent: 'center',
    },
    disabled: {
        opacity: 0.5,
        alignItems: 'center'
    },
    txtHeader: {
        fontSize: 40,
        fontFamily: 'Helvatica, sans-serif',
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 10,
        color: '#fff'
    },
    txtTitle: {
        fontWeight: 'bold',
        fontSize: 34,
        fontFamily: 'Helvatica, sans-serif',
        textAlign: 'center',
        marginTop: 80,
        color: '#fff'
    },
    header: {
        fontWeight: 'bold',
        marginBottom: 100,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#FFFFFF',
        flex: 1,
    },
    inputIcon: {
        width: 30,
        height: 30,
        marginLeft: 15,
        justifyContent: 'center'
    },
    inputLogo: {
        marginTop: 80,
        width: 300,
        height: 200,
        justifyContent: 'center'
    },
    profile: {
        width: 200,
        height: 200,
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,
    },
    signupButton: {
        backgroundColor: "gray",
    },
    signUpText: {
        color: 'white',
    },
    signinButton: {
        backgroundColor: "gray",
    },
    signinText: {
        color: 'white',
    },
    navigateSignin: {
        color: 'grey',
        paddingTop: 50
    },
    navigateSignup: {
        color: 'grey',
        paddingTop: 50
    },
    bottomWarning: {
        color: 'red',
        fontSize: 10
    },
    errorPopUp: {
        width: 300,
        height: 50,
        padding: 15,
        marginTop: 120,
        marginLeft: 30,
        marginBottom: 10,
        alignItems: 'center'
    },
    imageProfile: {
        width: 200,
        height: 200,
        borderRadius: 300,
        borderBottomWidth: 1,
        marginBottom: 20
    },

    topContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'lightblue',
    },
    buttomContainer: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    buttonContainer: {
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 20,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 30,
        width: 200,
        height: '20%',
        borderRadius: 50,
        backgroundColor: 'pink'
    },
    buttonTxt: {
        fontSize: 15
    },
    headerFeed: {
        height: 55,
        backgroundColor: '#ffd800',
        elevation: 5,
        flexDirection: 'row'
    },
    imgContainer: {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 20,
        borderRadius: 75,
        width: 150, height: 150,
    }

});
export default styles