import React, { Component } from 'react';
import store from './store/store';
import { Provider } from 'react-redux';
import MainNavigator from './component/MainNavigator';

export default class AppRedux extends Component {
    render() {
        return (
            <Provider store={store}>
                <MainNavigator />
            </Provider>
        );
    }
}

