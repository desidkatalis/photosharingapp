const Hapi = require('@hapi/hapi');
const Bcrypt = require('bcrypt');
const routeUser = require('../route/routeUser')
const routeFeed = require('../route/routeFeed')
const Path = require('path')
const Qs = require('qs')

const users = {
    desi: {
        username: 'desi',
        password: 'secret',
        name: 'Desi Suci',
        id: '2133d32a'
    }
};

const validate = async (request, username, password) => {

    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }

    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name };

    return { isValid, credentials };
};

const start = async () => {
    const server = Hapi.server({
        port: 1999,
        host: '0.0.0.0',
        query: {
            parser: (query) => Qs.parse(query)
        },
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'uploads')
            }
        }
    })

    await server.register(require('@hapi/basic'));
    await server.auth.strategy('simple', 'basic', { validate });

    await server.register({
        plugin: require('hapi-pino'),
        options: {
            prettyPrint: process.env.NODE_ENV !== 'production',
            stream: 'server.log',
            redact: ['req.headers.authorization']
        }
    });

    server.route(routeUser);
    server.route(routeFeed)

    server.start()
    console.log('Server running at:', server.info.uri);
    return server;
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

start()