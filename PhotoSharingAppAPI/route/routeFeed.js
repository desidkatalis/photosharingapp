const { postHandlers } = require('../handler/feedHandler')

const postFeed = [{
    method: 'POST',
    path: '/api/feed/{email}',
    handler: postHandlers.newPost
}]

const getFeed = [{
    method: 'GET',
    path: '/api/feed/{email}',
    handler: postHandlers.getFeeds
}]

const getFeedByEmail = [{
    method: 'GET',
    path: '/api/feed/{email}/post/{fileName}',
    handler: (req, h) => {
        return h.file(`${req.params.email}/post/${req.params.fileName}`)
    },
}]

module.exports = [].concat(getFeed, postFeed, getFeedByEmail)