const Joi = require('@hapi/joi')
const { checkAccount, createAccount, updateAccount, loginAccount } = require('../handler/userHandler')

const getAllUser = [{
    method: 'GET',
    path: '/api/user',
    handler: checkAccount
}]

const postUser = [{
    method: "POST",
    path: "/api/user",
    options: {
        validate: {
            payload: {
                email: Joi.string().required(),
                password: Joi.string().required(),
            }
        },
    },
    handler: createAccount
}];

const updateUser = [{
    method: 'PUT',
    path: '/api/user',
    handler: updateAccount,
    config: {
    }
}]

const loginUser = [{
    method: 'POST',
    path: '/api/user/login',
    handler: loginAccount,
    options: {
        validate: {
            payload: {
                email: Joi.string().required(),
                password: Joi.string().required(),
            }
        }
    }
}]

module.exports = [].concat(getAllUser, postUser, updateUser, loginUser)