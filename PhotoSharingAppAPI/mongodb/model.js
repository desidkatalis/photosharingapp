const { userScheme, feedSchema } = require('./schema')
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/photoSharingApp', {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})

mongoose.set('debug', true);

const userdb = mongoose.model('users', userScheme);
const feedDB = mongoose.model('feeds', feedSchema);

module.exports = { userdb, feedDB }