const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userScheme = new Schema(
    {
        id: Number,
        email: {
            type: String,
            unique: true
        },
        password: String,
        username: String,
        firstName: String,
        lastName: String,
        imageProfile: String,
        avatar: String,
        following: Array
    })

const feedSchema = new Schema({
    feedId: Number,
    tenant: String,
    imageUrl: String,
    caption: String,
    likes: Array,
    comments: Array,
    createdAt: Date,
    updatedAt: Date
})
module.exports = { userScheme, feedSchema }