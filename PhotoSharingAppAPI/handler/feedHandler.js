const fs = require('fs');
const { userdb, feedDB } = require('../mongodb/model');

const postHandlers = {
    newPost: async (req, h) => {
        const email = await userdb.findOne({ 'email': req.params.email }).lean()
        const imageData = req.payload.photoData

        if (req.params.email == email.email) {
            try {
                let timestamp = new Date().getTime();
                let dir = `./uploads/${email.email}/post/`
                let fileName = `${timestamp}.jpg`
                let postFeed = dir + fileName

                const payload = {
                    email: req.params.email,
                    postFeed: postFeed,
                    caption: req.payload.caption,
                    post_date: new Date(),
                }

                await feedDB.insertMany([payload])

                // create directory for images if not exist
                !fs.existsSync(dir) && fs.mkdirSync(dir, { recursive: true })

                fs.writeFile(postFeed, imageData, 'base64', function (err) {
                    if (err) {
                        throw new Error(err)
                    }
                });

                return h.response({ message: 'Your photo has been posted' }).code(201)
            } catch (err) {
                return h.response(err).code(400)
            }
        } else {
            return h.response({ error: 'An error occured.' })
        }
    },
    getFeeds: async (req, h) => {
        try {
            const email = req.params.email
            const result = await feedDB.find({ email: email }).lean()

            return h.response(result).code(200)
        } catch (err) {
            return h.response(err).code(400)
        }
    }
}

module.exports = { postHandlers }