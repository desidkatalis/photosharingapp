const { userdb } = require('../mongodb/model')
const fs = require('fs')
const btoa = require('btoa');

const checkAccount = function (request, h) {
    request.log('error', 'Event error');
    try {
        return userdb.find().lean()
    } catch (error) {
        return h.response({ message: 'Not found' }).code(404)
    }
}
const createAccount = async (req, h) => {
    try {
        const emailUser = await userdb.findOne({ "email": req.payload.email })

        if (emailUser === null) {
            const getId = await userdb.aggregate([{
                $group: {
                    _id: "max",
                    maxId: { $max: '$id' }
                }
            }])
            let nextId = getId.length > 0 ? parseInt(getId[0].maxId) + 1 : 1
            Object.assign(req.payload, { id: nextId, reg_date: new Date(), active: true })
            await userdb.insertMany(req.payload)
            return h.response({ status: 'Email successfully registered ' }).code(201)
        } else {
            return h.response({ status: ' Email failed to registered' }).code(400)
        }
    } catch (err) {
        return h.response(err).code(400)
    }
}

const updateAccount = async (request, h) => {
    const email = request.payload.email
    const timestamp = Date.now();
    const dir = `./public/img/${email}/avatar/`
    const fileName = `${timestamp}.jpg`
    const path = dir + fileName
    const { firstName, lastName, username, photoData } = request.payload
    try {
        !fs.existsSync(dir) && fs.mkdirSync(dir, { recursive: true })

        fs.writeFile(path, photoData, "base64", function (err) {
            if (err) throw new Error(err)
        });
        const update = await result.findOneAndUpdate({ email: email }, { firstName: firstName, lastName: lastName, username: username, avatar: path })
        return h.response(update).code(201)
    } catch (err) {
        throw new Error(err)
        return h.response(err).code(500)
    }
}

const loginAccount = async (req, h) => {
    const userLogin = await userdb.findOne({ "email": req.payload.email }).lean()

    if (userLogin !== null) {
        if (req.payload.password === userLogin.password) {
            let key = btoa(userdb.email, userLogin.password)
            return h.response({ auth: true, type: "Basic", key: key }).code(200)
        } else {
            return h.response({ auth: false })
        }
    } else {
        return h.response({ auth: false })
    }
}

module.exports = { checkAccount, createAccount, updateAccount, loginAccount }